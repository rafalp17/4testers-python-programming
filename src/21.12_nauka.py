def check_if_standard_conditions(temp_in_celsius, pressure_in_hpa):
    if temp_in_celsius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False


if __name__ == '__main__':
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa: {check_if_standard_conditions(0, 1013)}")
    print(f"Checking if standard conditions for 0 Celsius, 1014 hPa: {check_if_standard_conditions(0, 1014)}")
    print(f"Checking if standard conditions for 1 Celsius, 1013 hPa: {check_if_standard_conditions(1, 1013)}")
    print(f"Checking if standard conditions for 1 Celsius, 1014 hPa: {check_if_standard_conditions(1, 1014)}")