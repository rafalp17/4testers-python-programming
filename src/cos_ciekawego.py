# Importujemy moduł `math`
import math

# Definiujemy funkcję do obliczania średniego spalania
def oblicz_srednie_spalanie(spalanie_w_litrrach, przebieg):
    # Obliczamy średnie spalanie na 100 kilometrów
    srednie_spalanie_na_100_km = spalanie_w_litrrach / przebieg * 100

    # Zwracamy średnie spalanie na 100 kilometrów
    return srednie_spalanie_na_100_km

# Główna funkcja
def main():
    # Pobieramy od użytkownika spalone paliwo w litrach
    spalanie_w_litrrach = float(input("Podaj spalone paliwo w litrach: "))

    # Pobieramy od użytkownika ilość przejechanych kilometrów
    przebieg = float(input("Podaj ilość przejechanych kilometrów: "))

    # Obliczamy średnie spalanie
    srednie_spalanie_na_100_km = oblicz_srednie_spalanie(spalanie_w_litrrach, przebieg)

    # Wyświetlamy wynik
    print(f"Średnie spalanie na 100 kilometrów to:", (srednie_spalanie_na_100_km), "litrów.")

# Uruchamiamy program
if __name__ == "__main__":
    main()
