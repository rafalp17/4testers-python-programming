def multiply_by_three(a, b, c):
    return [a * 3, b * 3, c * 3]

if __name__ == '__main__':
    print(multiply_by_three(4, 5, 8))
