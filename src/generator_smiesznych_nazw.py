# Importujemy moduł random, który będzie służył do losowania fragmentów tekstu.
import random

# Definiujemy funkcję `generate_sentence()`, która będzie generować zdania.
def generate_sentence(sentence_fragments):

    # Funkcja przyjmuje listę fragmentów tekstu jako parametr.

    # Losowo wybieramy fragmenty tekstu z listy i łączymy je w jedno zdanie.
    fragments = random.sample(sentence_fragments, len(sentence_fragments))
    return " ".join(fragments)

# Definiujemy funkcję `main()`, która będzie wywoływać funkcję `generate_sentence()`.
def main():

    # Definiujemy listę fragmentów tekstu, które będziemy używać do generowania zdań.
    sentence_fragments = ["kot", "pies", "jadł", "spał", "biegł", "śpiący", "głodny", "leniwy"]

    # Wyświetlamy 10 zdań wygenerowanych przez funkcję `generate_sentence()`.
    for _ in range(10):
        print(generate_sentence(sentence_fragments))


# Sprawdzamy, czy program jest uruchamiany jako główny program.
if __name__ == "__main__":
    # Uruchamiamy funkcję `main()`.
    main()
    
input("jak chcesz zakończyć naciś enter")
