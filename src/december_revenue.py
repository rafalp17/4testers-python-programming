
def lista_obrotów(dzienny_obrot):
    sum_of_daily_revenue = sum(dzienny_obrot)
    sredni_obrot = sum_of_daily_revenue / len(dzienny_obrot)
    return sredni_obrot



if __name__ == '__main__':
    dzienny_obrot = [100, 230, 560, 330, 450, 800, 120, 150, 900, 770]
    sredni_obrot = lista_obrotów(dzienny_obrot)
    print(f"Średni obrót: {sredni_obrot}")

##################################################################################################
def średni_obrot(obroty):
    return sum(obroty) / len(obroty)


if __name__ == "__main__":
    obroty = [100, 230, 560, 330, 450, 800, 120, 150, 900, 770]
    sredni_obrot = średni_obrot(obroty)
    print(f"Średni obrót: {sredni_obrot}")