# Importujemy moduł math, który będzie służył do obliczeń matematycznych.

import math

# Definiujemy menu wyboru figury geometrycznej.
def menu():
    print("Wybierz figurę geometryczną, której pole powierzchni chcesz obliczyć:")
    print("1. Kwadrat")
    print("2. Prostokąt")
    print("3. Romb")
    print("4. Trapez")
    return int(input("Wpisz numer figury: "))

# Definiujemy funkcje obliczające pole powierzchni dla poszczególnych figur geometrycznych.
def pole_kwadratu(a):
    return a * a

def pole_prostokąta(a, b):
    return a * b

def pole_rombu(a, h):
    return a * h

def pole_trapezu(a, b, h):
    return (a + b) * h / 2

# Definiujemy funkcję główną programu.
def main():
    # Wyświetlamy menu wyboru figury geometrycznej.
    wybierz = menu()

    # W zależności od wyboru użytkownika, wywołujemy odpowiednią funkcję obliczającą pole powierzchni.
    if wybierz == 1:
        a = float(input("Podaj długość boku kwadratu: "))
        print("Pole powierzchni kwadratu wynosi:", pole_kwadratu(a))
    elif wybierz == 2:
        a = float(input("Podaj długość pierwszego boku prostokąta: "))
        b = float(input("Podaj długość drugiego boku prostokąta: "))
        print("Pole powierzchni prostokąta wynosi:", pole_prostokąta(a, b))
    elif wybierz == 3:
        a = float(input("Podaj długość boku rombu: "))
        h = float(input("Podaj wysokość rombu: "))
        print("Pole powierzchni rombu wynosi:", pole_rombu(a, h))
    elif wybierz == 4:
        a = float(input("Podaj długość podstawy górnej trapezu: "))
        b = float(input("Podaj długość podstawy dolnej trapezu: "))
        h = float(input("Podaj wysokość trapezu: "))
        print("Pole powierzchni trapezu wynosi:", pole_trapezu(a, b, h))
    else:
        print("Nieprawidłowy wybór figury geometrycznej.")

# Uruchamiamy funkcję główną programu.
if __name__ == "__main__":
    main()
