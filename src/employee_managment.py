from faker import Faker
import random

fake = Faker()


def generate_random_employee_dictionary():
    return {
        "email": fake.ascii_safe_email(),
        "seniority_years": random.randint(1, 40),
        "female": random.choice([True, False])
    }


if __name__ == '__main__':
    for n in range(100):
        print(generate_random_employee_dictionary())
