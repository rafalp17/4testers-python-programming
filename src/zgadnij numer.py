import random
def main():
  # Losujemy liczbę od 1 do 6
  liczba = random.randint(1, 100)

  # Informujemy użytkownika o zasadach gry
  print("Komputer losuje liczbę od 1 do 10. Ty musisz ją odgadnąć.")

  # Odgadujemy liczbę do skutku
  while True:
    # Pobieramy od użytkownika propozycję
    propozycja = int(input("Podaj propozycję: "))

    # Sprawdzamy, czy propozycja jest poprawna
    if propozycja == liczba:
      # Liczba została odgadnięta
      print("Gratulacje! Odgadłeś liczbę!")
      break
    elif propozycja < liczba:
      # Liczba jest za mała
      print("Liczba jest za mała.")
    else:
      # Liczba jest za duża
      print("Liczba jest za duża.")


if __name__ == "__main__":
  main()
