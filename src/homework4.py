def zadanie4(list):
    return list[len(list) // 2 + (len(list) % 2 - 1)]

if __name__ == '__main__':
    print(zadanie4([10, 3, 4, 11]))
    print(zadanie4([10, 3, 4, 11, 90]))