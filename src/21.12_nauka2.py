def get_grade_for_test_score(test_score):
    if test_score >= 90:
        return 5
    elif test_score >= 75:
        return 4
    elif test_score >= 50:
        return 3
    else:
        return 2

if __name__ == '__main__':
    print(f"Grade for 90% is: {get_grade_for_test_score(99)}")
    print(f"Grade for 76% is: {get_grade_for_test_score(76)}")
    print(f"Grade for 30% is: {get_grade_for_test_score(30)}")