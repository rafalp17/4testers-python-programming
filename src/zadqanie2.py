import datetime


def main():
  # Pobieramy od użytkownika imię
  imie = input("Jak masz na imię? ")

  # Pobieramy od użytkownika rok urodzenia
  rok_urodzenia = int(input("W którym roku się urodziłeś? "))

  # Obliczamy wiek
  obecny_rok = datetime.datetime.now().year
  wiek = obecny_rok - rok_urodzenia

  # Wypisujemy wiek
  print("Witaj, " + imie.capitalize() + ", masz " + str(wiek) + " lat.")


if __name__ == "__main__":
  main()




