if __name__ == '__main__':
    my_fiend = {
        "name": "Michal",
        "age": 33,
        "hobbies": ["ride_a_bike", "travel"]
    }

    my_fiend["name"] = "Paweł"
    my_fiend["age"] = my_fiend["age"] + 1
    my_fiend["age"] += 2
    my_fiend["hobbies"].append("archery")

    print(my_fiend)