import math


def main():
  # Pobieramy od użytkownika współrzędne kartezjańskie
  x = float(input("Podaj współrzędną x: "))
  y = float(input("Podaj współrzędną y: "))
  z = float(input("Podaj współrzędną z: "))

  # Przeliczamy współrzędne kartezjańskie na współrzędne sferyczne
  r = math.sqrt(x ** 2 + y ** 2 + z ** 2)
  teta = math.atan2(y, x)
  fi = math.acos(z / r)

  # Wypisujemy współrzędne sferyczne
  print("Współrzędne sferyczne:")
  print("r:", r)
  print("teta:", teta)
  print("fi:", fi)

  # Przeliczamy współrzędne kartezjańskie na współrzędne cylindryczne
  r = math.sqrt(x ** 2 + y ** 2)
  theta = math.atan2(y, x)
  z = z

  # Wypisujemy współrzędne cylindryczne
  print("Współrzędne cylindryczne:")
  print("r:", r)
  print("teta:", teta)
  print("z:", z)


if __name__ == "__main__":
  main()
