friend_first_name = "Paweł"
friend_age = 37
friend_numer_of_pets = 1
friend_has_driving_license = True
friendship_length_in_years = 12.5

print("My friend's name:", friend_first_name)
print("My friend's age:", friend_age)
print("My friend's number of pets:", friend_numer_of_pets)
print("My friend's driving license:", friend_has_driving_license)
print("How many years we've been friends:", friendship_length_in_years)
