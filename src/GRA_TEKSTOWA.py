import random

# Definiujemy stałe

KOLOR_ZAGADKI = "żółty"
KOLOR_ODPOWIEDZI = "niebieski"

# Definiujemy listę zagadek

zagadki = [
    "Co to jest zielone i skacze po drzewach?",
    "Co to jest białe i pływa w morzu?",
    "Co to jest czarne i ma cztery łapy?",
    "Co to jest czerwone i żółte?",
    "Co to jest duże i futrzaste?",
]

# Definiujemy listę odpowiedzi

odpowiedzi = [
    "żaba",
    "kaczka",
    "kot",
    "pomarańcza",
    "niedźwiedź",
]

# Definiujemy funkcję do wyświetlania tekstu w kolorze

def kolor(kolor):
    if kolor == "żółty":
        return "\033[33m"
    elif kolor == "niebieski":
        return "\033[34m"
    elif kolor == "reset":
        return "\033[0m"
    else:
        return ""

# Główna pętla gry

while True:

    # Losujemy zagadkę i odpowiedź
    zagadka = zagadki[random.randint(0, len(zagadki) - 1)]
    odpowiedz = odpowiedzi[random.randint(0, len(odpowiedzi) - 1)]

    # Wyświetlamy zagadkę
    print(kolor(KOLOR_ZAGADKI) + zagadka + kolor('reset'))

    # Pobieramy od użytkownika odpowiedź
    odpowiedz_uzytkownika = input("Twoja odpowiedź: ")

    # Sprawdzamy poprawność odpowiedzi
    if odpowiedz_uzytkownika.lower() == odpowiedz.lower():
        # Użytkownik odpowiedział poprawnie
        print("Brawo! Odpowiedziałeś poprawnie.")
    else:
        # Użytkownik odpowiedział niepoprawnie
        print("Niestety, Twoja odpowiedź jest błędna.")

