# Importujemy moduł random
import random

# Definiujemy funkcję do generowania słownika
def generate_dictionary(imiona, nazwiska, kraje):
    firstname = random.choice(imiona)
    lastname = random.choice(nazwiska)
    country = random.choice(kraje)
    email = firstname.lower() + "." + lastname.lower() + "@example.com"
    return {
        "firstname": firstname,
        "lastname": lastname,
        "country": country,
        "email": email,
    }

# Definiujemy funkcję do przyjmowania danych od użytkownika
def get_data():
    imiona = input("Podaj listę imion (oddzielonych średnikami): ")
    nazwiska = input("Podaj listę nazwisk (oddzielonych średnikami): ")
    kraje = input("Podaj listę krajów (oddzielonych średnikami): ")
    return imiona.split(" "), nazwiska.split(" "), kraje.split(" ")

# Pobieramy dane od użytkownika
imiona, nazwiska, kraje = get_data()

# Generujemy listę słowników
list_of_dictionaries = []
for i in range(10):
    list_of_dictionaries.append(generate_dictionary(imiona, nazwiska, kraje))

# Wyświetlamy listę słowników
for dictionary in list_of_dictionaries:
    print(dictionary["firstname"], dictionary["lastname"], dictionary["country"], dictionary["email"])
