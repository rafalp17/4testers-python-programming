import math


def main():
    # Pobieramy od użytkownika wartość promienia koła
    r = float(input("Podaj promień kola: "))

    if r < 0:
        print("To jest błędna wartość. Wpisz liczbę dodatnią lub równą 0")
        return

    # Przeliczamy pole oraz obwód kola
    P = math.pi * r ** 2
    L = 2 * math.pi * r

    print("Pole koła : ", P)
    print("Obwód koła:", L)

    input("Aby zakończyć naciśnij Enter")

if __name__ == '__main__':
    main()