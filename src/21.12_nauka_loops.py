def print_numbers_from_0_to_limit(limiting_number):
    for i in range(limiting_number + 1):
        print(i)


if __name__ == '__main__':
    print_numbers_from_0_to_limit(20)
