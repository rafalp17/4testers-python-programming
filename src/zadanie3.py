def main():
  # Pobieramy od użytkownika liczby a i b
  a = float(input("Podaj liczbę a: "))
  b = float(input("Podaj liczbę b: "))

  # Obliczamy sumę, różnicę, iloczyn i iloraz
  suma = a + b
  roznica = a - b
  iloczyn = a * b
  iloraz = a / b

  # Obliczamy pierwiastek z sumy liczb
  pierwiastek_sumy = (a + b) ** 0.5

  # Obliczamy a do potęgi b i b do potęgi a
  a_do_b = a ** b
  b_do_a = b ** a

  # Wypisujemy wyniki
  print("Suma: " + str(suma))
  print("Różnica: " + str(roznica))
  print("Iloczyn: " + str(iloczyn))
  print("Iloraz: " + str(iloraz))
  print("Pierwiastek z sumy: " + str(pierwiastek_sumy))
  print("a do potęgi b: " + str(a_do_b))
  print("b do potęgi a: " + str(b_do_a))


if __name__ == "__main__":
  main()
