import os
import json

def dodaj_rekord():
    # Pobieramy dane od użytkownika.
    imie = input("Podaj imię: ")
    nazwisko = input("Podaj nazwisko: ")
    telefon = input("Podaj numer telefonu: ")
    email = input("Podaj adres e-mail: ")

    # Tworzymy nowy rekord.
    rekord = {
        "imie": imie,
        "nazwisko": nazwisko,
        "telefon": telefon,
        "email": email
    }

    # Dodajemy rekord do książki adresowej.
    with open("ksiazka_adresowa.txt", "a") as plik:
        plik.write(json.dumps(rekord) + "\n")

def sortuj():
    # Otwieramy plik z książką adresową do odczytu.
    with open("ksiazka_adresowa.txt", "r") as plik:
        # Ładujemy dane z pliku do listy.
        dane = json.load(plik)

    # Sortujemy dane według wybranego kryterium.
    if sortowanie == "imie":
        dane.sort(key=lambda rekord: rekord["imie"])
    elif sortowanie == "nazwisko":
        dane.sort(key=lambda rekord: rekord["nazwisko"])
    else:
        print("Nieprawidłowy wybór kryterium sortowania.")

    # Zapisujemy posortowane dane do pliku.
    with open("ksiazka_adresowa.txt", "w") as plik:
        plik.write(json.dumps(dane))

def wyszukaj():
    # Pobieramy od użytkownika dane do wyszukania.
    kriterium = input("Podaj kryterium wyszukiwania: ")
    wartosc = input("Podaj wartość do wyszukania: ")

    # Otwieramy plik z książką adresową do odczytu.
    with open("ksiazka_adresowa.txt", "r") as plik:
        # Ładujemy dane z pliku do listy.
        dane = json.load(plik)

    # Wyszukiwanie danych.
    wyniki = [rekord for rekord in dane if kriterium in rekord[kriterium] and rekord[kriterium] == wartosc]

    # Wyświetlanie wyników.
    if not wyniki:
        print("Nie znaleziono żadnych wyników.")
    else:
        for wynik in wyniki:
            print(wynik)

def main():
    # Wyświetlamy menu programu.
    print("**Menu książki adresowej**")
    print("1. Dodaj rekord")
    print("2. Sortuj")
    print("3. Wyszukaj")
    print("4. Wyjdź")

    # Pobieramy od użytkownika wybór.
    wybierz = int(input("Wybierz opcję: "))

    # Wykonujemy wybraną opcję.
    if wybierz == 1:
        dodaj_rekord()
    elif wybierz == 2:
        print("Wybierz kryterium sortowania:")
        print("1. Imię")
        print("2. Nazwisko")
        sortowanie = input("Wybierz opcję: ")
        sortuj()
    elif wybierz == 3:
        wyszukaj()
    elif wybierz == 4:
        print("Do widzenia!")
        os.remove("ksiazka_adresowa.txt")
    else:
        print("Nieprawidłowy wybór.")

# Uruchamiamy funkcję główną programu.
if __name__ == "__main__":
    main()
