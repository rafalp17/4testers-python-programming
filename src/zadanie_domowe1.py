# Definiujemy funkcję `player_info()`. Funkcja ta przyjmuje jako argument słownik `player`, który zawiera informacje o graczu.
def player_info(player):

    # Tworzymy zmienną `player_data`, która będzie zawierać dane o graczu.
    player_data = f"The player {player['nick']} is of type {player['type']} and has {player['exp_points']} EXP"

    # Wyświetlamy dane o graczu.
    print(player_data)


# Uruchamiamy funkcję `player_info()`.
if __name__ == '__main__':

    # Definiujemy zmienną `player`, która zawiera informacje o graczu.
    player = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }

    # Wywołujemy funkcję `player_info()`, przekazując jej jako argument zmienną `player`.
    player_info(player)
