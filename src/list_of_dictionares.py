animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}

animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}

animal3 = {
    "kind": "snake",
    "age": 1,
    "male": True
}

animals = [{
    "kind": "snake",
    "age": 1,
    "male": True
    },
    {
    "kind": "dog",
    "age": 2,
    "male": True
    },
    {
    "kind": "cat",
    "age": 5,
    "male": False
    },
]

print(animals)
print(len(animals))
animals.append({
    "kind": "horse",
    "age": 10,
    "male": False
    },)
print(animals)
print(animals[2])
print(animals[-1])
print(animals[-1]["age"])
print(f"My animal is",animals[-1] ["kind"], "and it have",animals[-1]["age"], "age")
print(len(animals))