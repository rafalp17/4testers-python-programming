# Definiujemy funkcję `find_divisible_subset()`. Funkcja ta przyjmuje jako argumenty:
#   * `number_from`: minimalną liczbę w zakresie
#   * `number_to`: maksymalną liczbę w zakresie
#   * `divisible_by`: liczbę, przez którą mają być podzielne wyszukiwane liczby
# Funkcja zwraca listę liczb podzielnych przez `divisible_by` z zakresu od `number_from` do `number_to`.
def find_divisible_subset(number_from, number_to, divisible_by):

    # Tworzymy listę `result`, która będzie zawierać wyniki wyszukiwania.
    result = []

    # Iterujemy po liczbach z zakresu od `number_from` do `number_to`.
    for number in range(number_from, number_to + 1):

        # Sprawdzamy, czy liczba `number` jest podzielna przez `divisible_by`.
        if number % divisible_by == 0:

            # Jeśli tak, dodajemy liczbę `number` do listy `result`.
            result.append(number)

    # Zwracamy listę `result`.
    return result


# Uruchamiamy funkcję `find_divisible_subset()`.
if __name__ == "__main__":

    # Definiujemy zmienne `number_from`, `number_to` i `divisible_by`.
    number_from = 1
    number_to = 50
    divisible_by = 9

    # Wywołujemy funkcję `find_divisible_subset()`, przekazując jej jako argumenty zmienne `number_from`, `number_to` i `divisible_by`.
    result = find_divisible_subset(number_from, number_to, divisible_by)

    # Wyświetlamy wyniki wyszukiwania.
    print(result)
