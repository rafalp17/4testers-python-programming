# Definiujemy funkcję `convert_list_of_temperatures_in_celsius_to_fahrenheit()`.
# Funkcja ta przyjmuje jako argument listę temperatur w stopniach Celsjusza i zwraca listę odpowiadających im temperatur w stopniach Fahrenheita.
def concert_list_of_temperatures_in_celsius_to_fahrenheit(list_of_temps_in_celsius):

    # Tworzymy pustą listę `fahrenheit_temperatures`, która będzie zawierać wyniki konwersji.
    fahrenheit_temperatures = []

    # Iterujemy po temperaturze w Celsjuszu z listy `list_of_temps_in_celsius`.
    for temperature_in_celsius in list_of_temps_in_celsius:

        # Konwertujemy temperaturę z Celsjusza na Fahrenheita.
        temperature_in_fahrenheit = round(temperature_in_celsius * 1.8 + 32, 2)

        # Dodajemy przekonwertowaną temperaturę do listy `fahrenheit_temperatures`.
        fahrenheit_temperatures.append(temperature_in_fahrenheit)

    # Zwracamy listę temperatur w stopniach Fahrenheita.
    return fahrenheit_temperatures


# Uruchamiamy funkcję `convert_list_of_temperatures_in_celsius_to_fahrenheit()`.
if __name__ == "__main__":

    # Definiujemy listę temperatur w Celsjuszu.
    list_of_temps_in_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]

    # Konwertujemy temperatury z Celsjusza na Fahrenheita i wyświetlamy wyniki.
    print(concert_list_of_temperatures_in_celsius_to_fahrenheit(list_of_temps_in_celsius))
