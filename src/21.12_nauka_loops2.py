def print_numer_divisible_by_7(form_number, to_number):
    for i in range(form_number, to_number + 1):
        if i % 7 == 0:
            print(f"Number {i} is divisible by 7")


if __name__ == '__main__':
    print_numer_divisible_by_7(0, 35)