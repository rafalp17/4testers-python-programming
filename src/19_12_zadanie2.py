import random
import string


def generate_random_login_data(email):
    return {
        "email": email,
        "password": ''.join(random.sample(string.ascii_lowercase, 8))
    }

if __name__ == '__main__':
    print(generate_random_login_data("user@example.com"))