from cars import Car


def test_drive_increases_car_mileage():
    test_cars = Car("Fiat", "Red", 1988)
    test_cars.drive(100)
    test_cars.drive(100)
    assert test_cars.car_mileage == 200


def test_get_age_of_a_car():
    test_cars = Car("Fiat", "Red", 1988)
    assert test_cars.get_age() == 29


def test_repaint_the_car():
    test_cars = Car("Fiat", "Red", 1988)
    test_cars.repaint("Yellow")
    assert test_cars.color == "Yellow"
