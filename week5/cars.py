class Car:

    def __init__(self, brand, color, car_year, car_mileage):
        self.brand = brand
        self.color = color
        self.car_year = car_year
        self.car_mileage = 0

    def drive(self, distance):
        self.car_mileage += distance

    def get_age(self, car_year):
        return datatime.datetime.now().year - self.car_year

    def repaint(self, color):
        self.color = color


if __name__ == '__main__':
    car1 = Car("Fiat", "Red", 1988)
    car2 = Car("Tesla", "Blue", 1992)
    car3 = Car("RAM", "Black", 1991)
