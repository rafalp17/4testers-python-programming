def is_adult(age):
    if age >= 18:
        return True
    else:
        return False

# drugi test
def return_words_containing_letter_a(word_list):
    return [word for word in word_list if 'a' in word.lower()]
