class Dog:
    def __init__(self, name):
        self.name = name
        self.food_in_belly = 0

    def say_hello(self):
        print(f"Haau, Hau, my name is {self.name}")


    def feed(self, ammount_of_food):
        self.food_in_belly += ammount_of_food


if __name__ == '__main__':
    dog1 = Dog("Azor")
    dog2 = Dog("Burek")
    dog3 = Dog("Szarik")
    print(dog1.name)
    print(dog2.name)
    print(dog3.name)
    dog1.say_hello()
    dog2.say_hello()
    dog3.say_hello()

    dog1.feed(100)
    dog2.feed(89)
    dog3.feed(10)

    print(dog1.food_in_belly)
    print(dog2.food_in_belly)
    print(dog3.food_in_belly)