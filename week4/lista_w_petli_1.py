def map_numbers_to_squared(input_list):
    output_list = []

    for number in input_list:
        output_list.append(number ** 2)

    return output_list


if __name__ == '__main__':
    test_list = [1, 3, 5, 7, 9, 11, 13, 17, 23, 37]
    mapped_list = map_numbers_to_squared(test_list)
    print(mapped_list)