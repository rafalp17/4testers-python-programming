import random
import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

# Losowanie imion
random_gender = random.randint(0, 1)

# Generowanie listy słowników
people = [] # Ta linia inicjuje pustą listę o nazwie people, która będzie przechowywać słowniki reprezentujące 10 osób.
for i in range(10): # Ta pętla for iteruje 10 razy, generując jeden słownik osoby na iterację.

    # Generujemy słownik person
    if i < 5:
        random_gender = 0
    else:
        random_gender = 1
        # /\ Ten blok określa płeć osoby. Jeśli i jest mniejsze od 5, płeć jest ustawiona na 0 (kobieta), co oznacza użycie listy female_fnames.
        # W przeciwnym razie płeć jest ustawiona na 1 (mężczyzna), używając listy male_fnames.

    person = { # Ten blok tworzy słownik o nazwie person zawierający następujące klucze i wartości:
        "firstname": random.choice(female_fnames if random_gender == 0 else male_fnames),  # firstname: Losowo wybrane imię z odpowiadającej listy na podstawie wartości random_gender.
        "lastname": random.choice(surnames), # lastname: Losowo wybrane nazwisko z listy surnames.
        "country": random.choice(countries), # country: Losowo wybrany kraj z listy countries.
    }

    # Generujemy adres email na podstawie person
    email = f"{person['firstname'].lower()}.{person['lastname'].lower()}@example.com"
    person["email"] = email

    # Generujemy wiek, rok urodzenia i dorosłość na podstawie person
    age = random.randint(5, 45)
    person["age"] = age
    person["birth_year"] = datetime.datetime.now().year - age
    person["adult"] = person["age"] >= 18

    # Wypisujemy słownik w osobnej linijce
    print(person)
    people.append(person) # Ta linia dodaje słownik person do listy people, dodając go do listy 10 osób.

    # Generujemy przedstawianie osoby
    introduction = f"Hi! I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}."
    print(introduction)

