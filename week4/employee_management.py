import random #biblioteka random potrzebna do losowania liczb i plci

from faker import Faker # biblioteka faker potrzebna do generowania emaili nieprawdziwych

fake = Faker() # opcja wzieta z dokumentacji biblioteki faker


def generate_random_employee_dictionary():
    return {
        "email": fake.ascii_safe_email(), #generowanie z biblioteki faker (adresow email)
        "seniority_years": random.randint(1, 40), #ustawienie losowych liczb z przedzialu 1 do 40
        "female": random.choice([True, False]) #ustawienie losowego wybrania plci czy kobieta tak lub nie
    }

def generate_array_of_employee_dictionaries(number_of_dictionaries):
    return [generate_random_employee_dictionary() for _ in range(number_of_dictionaries)]

def get_emails_of_employees_with_seniority_years_greater_then_10(list_of_employees):
    output_emails = []


    for employee_dictionary in list_of_employees:
        if employee_dictionary["seniority_years"] > 10:
            output_emails.append(employee_dictionary["email"])

    return output_emails

if __name__ == '__main__':
    for n in range(2): # ustawienie zakresu generowania fakowych emaili w tym wypadku 10
        print(generate_random_employee_dictionary())
    print(generate_array_of_employee_dictionaries(3))

    generated_employees = generate_array_of_employee_dictionaries(20)
    print(generated_employees)

    filtered_emails = get_emails_of_employees_with_seniority_years_greater_then_10(generated_employees)
    print(filtered_emails)