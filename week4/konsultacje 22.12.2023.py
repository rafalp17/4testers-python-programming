def check_year_warranty(production_year, total_distance):
    """
    Sprawdza, czy pojazd nadal ma gwarancję.

    Args:
        production_year: Rok produkcji pojazdu.
        total_distance: Przebieg pojazdu w kilometrach.

    Returns:
        True, jeśli pojazd nadal ma gwarancję, False w przeciwnym razie.
    """

    age = 2023 - production_year
    return age <= 5 and total_distance <= 60000


def main():
    """
    Główna funkcja programu.

    Promptuje użytkownika o podanie roku produkcji i przebiegu pojazdu, a następnie sprawdza, czy pojazd nadal ma gwarancję.
    """

    production_year = int(input("Podaj rok produkcji samochodu: "))
    total_distance = int(input("Podaj przebieg samochodu: "))

    if check_year_warranty(production_year, total_distance):
        print("Twój pojazd nadal ma gwarancję! Gratulacje!")
    else:
        print("Twój pojazd nie ma już gwarancji.")


if __name__ == "__main__":
    main()
